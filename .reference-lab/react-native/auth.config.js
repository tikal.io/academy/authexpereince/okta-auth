export default {
  oidc: {
    clientId: '0oa16yvnrfCmFzC0x5d7',
    redirectUri: 'com.okta.dev-5427317:/callback',
    endSessionRedirectUri: 'com.okta.dev-5427317:/callback',
    discoveryUri: 'https://dev-5427317.okta.com/oauth2/default',
    scopes: ['openid', 'profile', 'offline_access'],
    requireHardwareBackedKeyStore: false,
  },
};

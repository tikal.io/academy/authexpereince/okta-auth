# Getting started with Okta

## Prequisites

- Pakage Manager:
  - Mac:
  - Windows:
  - Linux: 
  
- Important! - A **valid e-mail** - 

## Create an okta account

### CLI (preferred)

- [Installations instructions](./docs/cli/README.md)

An exmaple `okta start` would look like:

```sh
okta start
Registering for a new Okta account, if you would like to use an existing account, use 'okta login' instead.

First name: Auth
Last name: Expereince-Try1
Email address: authex-okta1@tikalk.dev
Company: tikal-knowledge
Creating new Okta Organization, this may take a minute:
OrgUrl: https://dev-5427317.okta.com
An email has been sent to you with a verification code.

Check your email
```

- Verify the account with a token you recieved via `e-mail`

Once entered the prompt should yield the following - please just `^C` (Ctrl-c) so we can continue the tutorial after login:

```sh
New Okta Account created!
Your Okta Domain: https://dev-5427317.okta.com
To set your password open this link:
https://dev-5427317.okta.com/welcome/drplQD_ThTMxjjIa9Rcn

Select a sample
> 1: Spring Boot + Okta
> 2: Vue + Okta
> 3: ASP.NET Core MVC + Okta
> 4: Angular + Okta
> 5: React Native + Okta
> 6: React + Okta
Enter your choice [Spring Boot + Okta]:
```

### Alternatively - Via Web

- [Start Here](https://developer.okta.com/signup)

---

# React app using okta-cli

## :compass: check-point #1 

**Ensure** you can login to https://dev-${YOUR_ORG}-admin.okta.com

### 1. Create a token

- Via web, Get <your-org-id> 

```sh
export YOUR_ORG=10970698
open https://dev-${YOUR_ORG}-admin.okta.com/admin/access/api/tokens
```

### 2. OKTA login & Create app

- 2.1 OKTA login

```sh
okta login
Okta Org URL: https://dev-5427317.okta.com
Enter your Okta API token, for more information see: https://bit.ly/get-okta-api-token
Okta API token: 00oY3CFa95090IukmmlRGLiYfdWnSVxVsuEXq-3vSq
```

- 2.2 Creating a React based SPA

```sh
okta start react
```

Should yield (if logged-in):

```sh
Configuring a new OIDC Application, almost done:
Created OIDC application, client-id: 0oa16xr8eiYpMTN3n5d7
|
Change the directory:
    cd react

Okta configuration written to: .okta.env

Build this example using NPM:

    npm install

And run with:

    npm start
```

- 2.3 install deps

```sh
cd react && npx npm install
```

- 2.4 Start react app

```sh
npx npm start
```

- 2.5 Login to your app with the username/pass you used to create the account

## 3. Review the application you created via CLI

The link should include your orgID: https://dev-<orgID>-admin.okta.com/admin/apps/active

Link on the `okta-react-sample`.

## 4. Review `src/.okta.env`

This is the file the cli generated with your `ISSUER_URL` and `ClientID` 

- :bulb: we will discuss what's missing here in the flow in the next LAB

## 5. Review `src/app.js`

note:

```javascript
import { OktaAuth, toRelativeUrl } from '@okta/okta-auth-js';
import { Security, SecureRoute, LoginCallback } from '@okta/okta-react';
```

Note how the okta-auth-js authenitcates via `/login/callback` route

```javascript
<Switch>
  <Route path="/" exact={true} component={Home}/>
  <Route path="/login/callback" component={LoginCallback}/>
  <SecureRoute path="/messages" component={Messages}/>
  <SecureRoute path="/profile" component={Profile}/>
</Switch>
```

This flow uses the TOKEN in the `.okta.env`, this standard JWT token allows you to view information on the user - see -> http://localhost:3000/profile

- In the next lab we will use a backend based onnode.js to act as our resource server